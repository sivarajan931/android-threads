package com.siva.threaddemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public class mythread extends Thread{
        TextView tv1;
        int n;
        @Override
        public void run() {
            runOnUiThread(() -> {
                String f = getName() + " has started";
                tv1.setText(f);
            });
            try {
                sleep(n);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            runOnUiThread(() -> {
                String f = getName() + " has quit";
                tv1.setText(f);
            });

        }
        mythread(String name, TextView t, int delay){
            tv1 = t;
            super.setName(name);
            n = delay;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mythread t1 = new mythread("Thread 1", findViewById(R.id.tv1),3000);
        mythread t2 = new mythread("Thread 2", findViewById(R.id.tv2),1000);

        Button b1 = findViewById(R.id.button);
        b1.setOnClickListener(view->{


            t1.start();
            t2.start();

        });
    }
}